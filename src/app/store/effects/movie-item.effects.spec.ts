import {TestBed} from '@angular/core/testing';
import {provideMockActions} from '@ngrx/effects/testing';
import {Observable, of, ReplaySubject} from 'rxjs';

import {MovieItemEffects} from './movie-item.effects';
import {MovieService} from '../../services/movie.service';
import {Movie} from '../../models/movie.model';
import {genreType} from '../../models/genre-type.model';
import {MovieItemLoad, MovieItemSuccess} from '../actions/movie-item.actions';

class MockMovieService {
  public static mockMovieData: Movie = {
    id: 20,
    key: 'the-dark-knight',
    name: 'The Dark Knight',
    description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
    genres: [genreType.action, genreType.crime, genreType.drama],
    rate: 9.0,
    length: '2hr 32mins',
    img: 'the-dark-knight.jpg'
  };

  get(id: number): Observable<Movie> {
    return of(MockMovieService.mockMovieData);
  }
}

describe('MovieItemEffects', () => {
  let actions$: ReplaySubject<any>;
  let effects: MovieItemEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MovieItemEffects,
        provideMockActions(() => actions$),
        {provide: MovieService, useClass: MockMovieService}
      ]
    });

    effects = TestBed.get(MovieItemEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });

  it('should return correct answer and type', () => {
    actions$ = new ReplaySubject(1);
    actions$.next(new MovieItemLoad({id: 1}));

    effects.loadMovieItem$.subscribe(result => {
      expect(result).toEqual(new MovieItemSuccess({movie: MockMovieService.mockMovieData}));
    });
  });
});
