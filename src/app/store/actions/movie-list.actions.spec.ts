import {genreType} from '../../models/genre-type.model';
import {MovieListActionTypes, MovieListError, MovieListLoad, MovieListSuccess} from './movie-list.actions';

describe('MovieList Actions', () => {
  describe('MovieListLoad Action', () => {
    it('should create an action', () => {
      const payload = {query: 'q', genre: genreType.action};
      const action = new MovieListLoad(payload);

      expect({...action}).toEqual({
        type: MovieListActionTypes.MOVIE_LIST_LOAD,
        payload
      });
    });
  });

  describe('MovieListSuccess Action', () => {
    it('should create an action', () => {
      const payload = {
        movies: [
          {
            id: 20,
            key: 'the-dark-knight',
            name: 'The Dark Knight',
            description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
            genres: [genreType.action, genreType.crime, genreType.drama],
            rate: 9.0,
            length: '2hr 32mins',
            img: 'the-dark-knight.jpg'
          }
        ]
      };
      const action = new MovieListSuccess(payload);

      expect({...action}).toEqual({
        type: MovieListActionTypes.MOVIE_LIST_SUCCESS,
        payload
      });
    });
  });

  describe('MovieListError Action', () => {
    it('should create an action', () => {
      const payload = {error: {name: 'Error name', message: 'Error message', stack: 'Error stack'}};
      const action = new MovieListError(payload);

      expect({...action}).toEqual({
        type: MovieListActionTypes.MOVIE_LIST_ERROR,
        payload
      });
    });
  });
});
