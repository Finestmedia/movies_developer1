import {reducer, initialState} from './movie-list.reducer';
import {MovieListError, MovieListLoad, MovieListSuccess} from '../actions/movie-list.actions';
import {Movie} from '../../models/movie.model';
import {genreType} from '../../models/genre-type.model';

describe('Movie Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('MOVIE_LIST_LOAD action', () => {
    it('should set loading to true', () => {
      const action = new MovieListLoad({query: 'q', genre: 'action'});
      const state = reducer(initialState, action);

      expect(state.isLoading).toEqual(true);
      expect(state.error).toEqual(null);
      expect(state.collection).toEqual([]);
    });
  });

  describe('MOVIE_LIST_SUCCESS action', () => {
    it('should set movie collection correct', () => {
      const expectedMovie: Movie = {
        id: 20,
        key: 'the-dark-knight',
        name: 'The Dark Knight',
        description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
        genres: [genreType.action, genreType.crime, genreType.drama],
        rate: 9.0,
        length: '2hr 32mins',
        img: 'the-dark-knight.jpg'
      };

      const action = new MovieListSuccess({movies: [expectedMovie]});
      const state = reducer(initialState, action);

      expect(state.isLoading).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.collection).toEqual([expectedMovie]);
    });
  });

  describe('MOVIE_LIST_ERROR action', () => {
    it('should set error correct', () => {
      const expectedError: Error = {name: 'Error name', message: 'Error message', stack: 'Error stack'};

      const action = new MovieListError({error: expectedError});
      const state = reducer(initialState, action);

      expect(state.isLoading).toEqual(false);
      expect(state.error).toEqual(expectedError);
      expect(state.collection).toEqual([]);
    });
  });
});
