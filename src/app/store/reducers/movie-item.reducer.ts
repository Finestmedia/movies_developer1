import {MovieItemActions, MovieItemActionTypes} from '../actions/movie-item.actions';
import {Movie} from '../../models/movie.model';

export interface MovieItemState {
  item: Movie;
  isLoading: boolean;
  error: Error | null;
}

export const initialState: MovieItemState = {
  item: null,
  isLoading: false,
  error: null
};

export function reducer(state = initialState, action: MovieItemActions): MovieItemState {
  switch (action.type) {
    case MovieItemActionTypes.MOVIE_ITEM_LOAD: {
      return {
        ...state,
        error: null,
        isLoading: true,
      };
    }
    case MovieItemActionTypes.MOVIE_ITEM_SUCCESS: {
      return {
        ...state,
        item: action.payload.movie,
        error: null,
        isLoading: false
      };
    }
    case MovieItemActionTypes.MOVIE_ITEM_ERROR: {
      return {
        ...state,
        error: action.payload.error,
        isLoading: false
      };
    }
    default:
      return state;
  }
}
