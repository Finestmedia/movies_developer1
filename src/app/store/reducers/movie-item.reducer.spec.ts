import {reducer, initialState} from './movie-item.reducer';
import {MovieItemError, MovieItemLoad, MovieItemSuccess} from '../actions/movie-item.actions';
import {Movie} from '../../models/movie.model';
import {genreType} from '../../models/genre-type.model';

describe('MovieItem Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;
      const state = reducer(initialState, action);

      expect(state).toBe(initialState);
    });
  });

  describe('MOVIE_ITEM_LOAD action', () => {
    it('should set loading to true', () => {
      const action = new MovieItemLoad({id: 1});
      const state = reducer(initialState, action);

      expect(state.isLoading).toEqual(true);
      expect(state.error).toEqual(null);
      expect(state.item).toEqual(null);
    });
  });

  describe('MOVIE_ITEM_SUCCESS action', () => {
    it('should set movie item correct', () => {
      const expectedMovie: Movie = {
        id: 20,
        key: 'the-dark-knight',
        name: 'The Dark Knight',
        description: 'When the menace known as the Joker wreaks havoc and chaos on the people of Gotham, ...',
        genres: [genreType.action, genreType.crime, genreType.drama],
        rate: 9.0,
        length: '2hr 32mins',
        img: 'the-dark-knight.jpg'
      };

      const action = new MovieItemSuccess({movie: expectedMovie});
      const state = reducer(initialState, action);

      expect(state.isLoading).toEqual(false);
      expect(state.error).toEqual(null);
      expect(state.item).toEqual(expectedMovie);
    });
  });

  describe('MOVIE_ITEM_ERROR action', () => {
    it('should set error correct', () => {
      const expectedError: Error = {name: 'Error name', message: 'Error message', stack: 'Error stack'};

      const action = new MovieItemError({error: expectedError});
      const state = reducer(initialState, action);

      expect(state.isLoading).toEqual(false);
      expect(state.error).toEqual(expectedError);
      expect(state.item).toEqual(null);
    });
  });
});
