import {GenreType} from './genre-type.model';

export interface Movie {
  id: number;
  key: string;
  name: string;
  description: string;
  genres: GenreType[];
  rate: number;
  length: string;
  img: string;
}
