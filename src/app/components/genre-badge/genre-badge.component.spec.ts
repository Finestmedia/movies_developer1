import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {GenreBadgeComponent} from './genre-badge.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {AppRoutingModule} from '../../app-routing.module';
import {MovieListComponent} from '../movie-list/movie-list.component';
import {MovieItemComponent} from '../movie-item/movie-item.component';
import {SpinnerComponent} from '../spinner/spinner.component';
import {ErrorMessageComponent} from '../error-message/error-message.component';

describe('GenreBadgeComponent', () => {
  let component: GenreBadgeComponent;
  let fixture: ComponentFixture<GenreBadgeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        GenreBadgeComponent,
        MovieListComponent,
        MovieItemComponent,
        SpinnerComponent,
        ErrorMessageComponent
      ],
      imports: [
        FormsModule,
        RouterModule,
        AppRoutingModule
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GenreBadgeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render badges correct', () => {
    const expectedNumberOfGenre = 11;
    const firstGenreName = 'action';

    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelectorAll('a').length).toBe(expectedNumberOfGenre);
    expect(compiled.querySelector('a').textContent).toContain(firstGenreName);
  });
});
