import {Component, Input} from '@angular/core';
import {GenreType, genreType} from '../../models/genre-type.model';

@Component({
  selector: 'app-genre-badge',
  templateUrl: './genre-badge.component.html',
  styleUrls: ['./genre-badge.component.sass']
})
export class GenreBadgeComponent {
  @Input()
  public activeGenre: GenreType;

  public allGenres = genreType;

  constructor() {
  }
}
