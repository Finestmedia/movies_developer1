import {Component} from '@angular/core';
import {fadeAnimation} from './animations/fade.animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass'],
  animations: [fadeAnimation]
})
export class AppComponent {
  public routeAnimation(outlet) {
    return outlet.isActivated ? outlet.activatedRoute : '';
  }
}
